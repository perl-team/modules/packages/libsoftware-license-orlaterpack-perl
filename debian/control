Source: libsoftware-license-orlaterpack-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Axel Beckert <abe@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libmodule-build-tiny-perl,
               perl
Build-Depends-Indep: libscalar-list-utils-perl <!nocheck>,
                     libsoftware-license-perl <!nocheck>,
                     libtest-diaginc-perl <!nocheck>,
                     libtest-simple-perl <!nocheck>,
                     libversion-perl <!nocheck>
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libsoftware-license-orlaterpack-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libsoftware-license-orlaterpack-perl.git
Homepage: https://metacpan.org/release/Software-License-OrLaterPack
Rules-Requires-Root: no

Package: libsoftware-license-orlaterpack-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libsoftware-license-perl,
         libversion-perl
Enhances: libdist-zilla-perl,
          libsoftware-license-perl
Description: Use GNU licenses with "or later" clause in Software::License
 Software::License::OrLaterPack allows one to use GNU licenses with
 "or later" clause where Software::License style license names are
 supported.
 .
 All "or later" are just subclasses of corresponding base license
 classes. For example, Software::License::GPL_3::or_later is a
 subclass of Software::License::GPL_3, so any "or later" license can
 be used like any other license. For example, in your dist.ini file:
 .
   license = GPL_3::or_later
 .
 However, licenses in the pack introduce few features not found in
 base classes.
